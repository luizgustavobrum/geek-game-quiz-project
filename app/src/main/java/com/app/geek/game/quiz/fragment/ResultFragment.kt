package com.app.geek.game.quiz.fragment


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.app.geek.game.quiz.Data.Companion.EXTRA_INDEX
import com.app.geek.game.quiz.Data.Companion.EXTRA_MESSAGE
import com.app.geek.game.quiz.R
import kotlinx.android.synthetic.main.fragment_result.*

class ResultFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_result, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val index = arguments?.getString(EXTRA_INDEX) as String
        val message = arguments?.getString(EXTRA_MESSAGE) as String

        text_result.text = message

        button.setOnClickListener {
            this.findNavController().popBackStack()
            this.findNavController().navigate(R.id.main_fragment)
        }
    }

}
