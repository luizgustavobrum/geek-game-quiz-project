package com.app.geek.game.quiz.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.app.geek.game.quiz.R
import com.blogspot.atifsoftwares.animatoolib.Animatoo

class SplashActivity : AppCompatActivity() {

    private val splashTime = 2000L
    private lateinit var myHandler: Handler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        initHandler()
    }

    private fun startToMainActivity(){
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        Animatoo.animateFade(this)
        this.finish()
    }

    private fun initHandler(){
        myHandler = Handler()
        myHandler.postDelayed({
            startToMainActivity()
        }, splashTime)
    }
}
