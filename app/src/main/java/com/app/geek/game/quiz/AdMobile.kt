package com.app.geek.game.quiz

import android.content.Context
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.google.android.gms.ads.MobileAds

class AdMobile(private val context: Context) {

    private lateinit var mInterstitialAd: InterstitialAd

    fun initializeAd(){
        MobileAds.initialize(context,
            "ca-app-pub-3940256099942544~3347511713")
        mInterstitialAd = InterstitialAd(context)
        mInterstitialAd.adUnitId = "ca-app-pub-3940256099942544/1033173712"
        mInterstitialAd.loadAd(AdRequest.Builder().build())
        mInterstitialAd.adListener = object : AdListener() {
            override fun onAdClosed() {
                mInterstitialAd.loadAd(AdRequest.Builder().build())
            }
        }
    }

    fun showAd(){
        if (mInterstitialAd.isLoaded){
            mInterstitialAd.show()
        }
    }


}