package com.app.geek.game.quiz

import kotlin.random.Random

fun title(): String{
    val titles =
        listOf("Estamos preparados, capitão!\nInicie uma partida.",
            "Só mais um round!\nComece uma partida.",
            "O fracasso não é razão para você desistir,\ndesde que continue acreditando.\nJogue outra vez!",
            "O meu dever é lutar!\nComece uma partida.",
            "O céu acordou e eu também.\nA gente tem que brincar!\nInicie um jogo.")
    val randon = Random.nextInt(0, titles.size)
    val title = titles[randon]
    return title
}

fun findQuestions(): List<Data>{

    val data1 = Data(1, "Qual foi o primeiro ator a interpretar o Hulk numa adaptação cinematográfica?", "Mark Ruffalo", "Lou Ferrigno", "Edward Norton", "Eric Bana", "Lou Ferrigno")
    val data2 = Data(2, "Quem foi o criador da obra Naruto?", "Weekly Shōnen Jump", "Yoshihiro Togashi", "Eiichiro Oda", "Masashi Kishimoto", "Masashi Kishimoto")
    val data3 = Data(3, "Qual o nome da mãe biológica de Clark Kent?", "Lois Lane", "Lara Lor-Van", "Martha Kent", "Kara Zor-E", "Lara Lor-Van")
    val data4 = Data(4, "Em que ano foi lançado a clássica adaptção animada dos X-men?", "1992", "1993", "1994", "1995", "1992")
    val data5 = Data(5, "Em que ano foi lançado o primeiro filme do universo cinematografico da marvel?", "2007", "2008", "2009", "2010", "2008")
    val data6 = Data(6, "Qual foi o primeiro filme de Super-Herói a passar a marca de 1 bilhão de dolares em arregadação?", "Marvel`s Vingadores", "Homem-Aranha 3", "Vingadores: Era de Ultron", "Batman: O Cavaleiro das trevas", "Batman: O Cavaleiro das trevas")
    val data7 = Data(7, "Qual é o nome do local que Luke Skywalker fica isolado na trilogia sequel?", "Ahch-To", "Jakku", "Crait", "kamino", "Ahch-To")
    val data8 = Data(8, "Em Star Wars: O Despertar da Força, onde Rey morava?", "Em um Star Destroyer", "Em um At-At", "Em um X-wing", "Em uma TIE Figther", "Em um At-At")
    val data9 = Data(9, "Qual é a cor do sabre de luz de Rey em Star Wars?", "Amarelo", "Azul", "Roxo", "Verde", "Amarelo")
    val data10 = Data(10, "Qual é o nome do diretor de Star Wars: O Retorno de Jedi?", "George Lucas", "Irvin Kershner", "Richard Marquand", "J. J. Abrams", "Richard Marquand")
    val data11 = Data(11, "Qual foi o outro filme dirigido pelo diretor do live action de Mogli: O Menino Lobo?", "Malévola", "O Rei Leão", "Dumbo", "Aladdin", "O Rei Leão")
    val data12 = Data(12, "Em que ano foi fundada a Netflix? ", "1997", "2003", "2009", "2015", "1997")
    val data13 = Data(13, "Qual foi o outro filme dirigido pelo diretor de Star Wars: O Despertar da Força?", "Baby Driver", "Cloverfield", "Super 8", "Whiplash", "Super 8")
    val data14 = Data(14, "Qual é o nome do apresentador de talkshow que o Coringa mata no seu filme?", "James Franklin", "Joseph Franklin", "Murray Franklin", "Noah Franklin", "Murray Franklin")
    val data15 = Data(15, "Qual ator interpreta o personagem Charlie Barber no filme Marriage Story?", "Ryan Gosling", "Adam Driver", "Miles Teller", "Domhnall Gleeson", "Adam Driver")
    val data16 = Data(16, "Quantos personagens levantaram o Mjolnir nos filmes da Marvel?", "1", "2", "3", "4", "3")
    val data17 = Data(17, "Qual o nome do local em que Luke Skywalker treinou no quinto filme da franquia?", "Tatooine", "Mustafar", "Exegol", "Dagobah", "Dagobah")
    val data18 = Data(18, "Quantos oscars o filme Avatar ganhou?", "3", "5", "8", "9", "3")
    val data19 = Data(19, "Qual o ator que interpretou o Batman em Batman Eternamente?", "Christian Bale", "George Clooney", "Val Kilmer", "Michael Keaton", "Val Kilmer")
    val data20 = Data(20, "Quem venceu o oscar de melhor animação em 2010?", "Big Hero 6", "Toy Story 3", "Up", "Frozen", "Up")
    val data21 = Data(21, "Em Harry Potter e o Enigma do Príncipe, qual foi o feitiço feito por Harry para atacar Snape no final do filme?", "Avada Kedavra", "Expelliarmus", "Sectumsempra", "Expecto Patronum", "Sectumsempra")
    val data22 = Data(22, "Qual dos filhos de Thanos é o primeiro a morrer em Guerra Infinita?", "Corvus Glaive", "Fauce de Ébano", "Próxima Meia Noite", "Cull Obisidian", "Fauce de Ébano")
    val data23 = Data(23, "Em que ano foi lançado a primeira edição das Crónicas de Gelo e Fogo?", "1994", "1995", "1996", "1997", "1996")
    val data24 = Data(24, "Qual é o ator principal de Jurassic World?", "Chris Pratt", "Chris Evans", "Chris Pine", "Chris Hemsworth", "Chris Pratt")
    val data25 = Data(25, "Qual é o nome da cidade que se passa a História de Stranger Things?", "Sleepy Hollow", "Hawkins", "Sunnydale", "Mystic Falls", "Hawkins")

    val list = listOf(
        data1, data2, data3, data4, data5,
        data6, data7, data8, data9, data10,
        data11, data12, data13, data14, data15,
        data16, data17, data18, data19, data20,
        data21, data22, data23, data24, data25) as MutableList<Data>

    return list.shuffled()

}