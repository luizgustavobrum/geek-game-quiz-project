package com.app.geek.game.quiz

data class Data(
    val index : Int,
    val question: String,
    val answerOne : String,
    val answerTwo: String,
    val answerThree: String,
    val answerFour: String,
    val answer: String
){
    companion object {
        val EXTRA_MESSAGE = "MESSAGE"
        val EXTRA_INDEX = "INDEX"
    }
}