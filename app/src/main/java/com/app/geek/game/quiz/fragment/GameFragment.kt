package com.app.geek.game.quiz.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.fragment.findNavController
import com.app.geek.game.quiz.AdMobile
import com.app.geek.game.quiz.Data
import com.app.geek.game.quiz.Data.Companion.EXTRA_INDEX
import com.app.geek.game.quiz.Data.Companion.EXTRA_MESSAGE
import com.app.geek.game.quiz.R
import com.app.geek.game.quiz.findQuestions
import kotlinx.android.synthetic.main.fragment_game.*

class GameFragment : Fragment() {

    private lateinit var adMobile : AdMobile
    private lateinit var list : List<Data>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_game, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        list = findQuestions()
        adMobile = AdMobile(requireContext())
        adMobile.initializeAd()
        startGame(0)
    }

    private fun startGame(index : Int){
        if (index < list.size){

            val question = list[index]
            text_question.text = question.question
            button_text_option_1.text = question.answerOne
            button_text_option_2.text = question.answerTwo
            button_text_option_3.text = question.answerThree
            button_text_option_4.text = question.answerFour

            onClickListenerCustom(button_text_option_1,question.answerOne, question.answer, index )
            onClickListenerCustom(button_text_option_2,question.answerTwo, question.answer, index )
            onClickListenerCustom(button_text_option_3,question.answerThree, question.answer, index )
            onClickListenerCustom(button_text_option_4,question.answerFour, question.answer, index )

        }else{
            adMobile.showAd()
            toResultFragment(index, this.getString(R.string.text_vitoria))
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun onClickListenerCustom(button: Button, answerQuestion: String, answer: String, index: Int){
        button.setOnClickListener {
            showResult(answerQuestion, answer, index)
        }
    }

    private fun showResult(answerQuestion: String, answer: String, index: Int) {
        when(answer.trim()){
            answerQuestion.trim() -> {
                startGame(index + 1)
            }
            else ->{
                adMobile.showAd()
                toResultFragment(index, this.getString(R.string.text_derrota))
            }
        }
    }

    private fun toResultFragment(index: Int, message: String){
        this.findNavController().popBackStack()
        this.findNavController().navigate(R.id.result_fragment, setBundle(index, message))
    }

    private fun setBundle(index: Int, message: String) : Bundle{
        val bundle = Bundle()
        bundle.putString(EXTRA_MESSAGE, message)
        bundle.putString(EXTRA_INDEX, index.toString())
        return bundle
    }
}
